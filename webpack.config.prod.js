const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
module.exports = {
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()]
  }
};

const config = {
  mode: "production",
  entry: {
    app: "./index.js"
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].[contenthash:8].js"
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.s?css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              importLoaders: 2
            }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: [require("autoprefixer")()]
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(svg|png|jpg|woff2)$/,
        loader: "file-loader",
        options: {
          name: "[name].[contenthash:8].[ext]"
        }
      }
    ]
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: "index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].[contenthash:8].css"
    })
  ]
};

module.exports = config;
