const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require("path");

const config = {
  mode: "development",
  entry: {
    app: "./index.js"
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  devServer: {
    hotOnly: true,
    contentBase: path.resolve(__dirname, "public")
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.s?css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              importLoaders: 2
            }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: [require("autoprefixer")()]
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(svg|png|jpg|woff2)$/,
        loader: "file-loader"
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "index.html"
    })
  ]
};

module.exports = config;
