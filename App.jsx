import { hot } from "react-hot-loader/root";
import React from "react";
import "./style.css";

function block() {
  const block = [
    {
      image: "https://source.unsplash.com/320x420/?architecture",
      headline: "Архитектура",
      description: "Оно зачем-то это построили"
    },
    {
      image: "https://source.unsplash.com/320x420/?architecture",
      headline: "Архитектура",
      description: "Они зачем-то это построили"
    },
    {
      image: "https://source.unsplash.com/320x420/?architecture",
      headline: "Архитектура",
      description: "Они зачем-то это построили"
    },
    {
      image: "https://source.unsplash.com/320x420/?architecture",
      headline: "Архитектура",
      description: "Они зачем-то это построили"
    },
    {
      image: "https://source.unsplash.com/320x420/?architecture",
      headline: "Архитектура",
      description: "Они зачем-то это построили"
    }
  ];
  return block.map(block => {
    return (
      <div class="block">
        <img src={block.image}></img>
        <div class="headline">{block.headline}</div>
        <div class="description">{block.description}</div>
      </div>
    );
  });
}

function App() {
  return (
    <div className="bg_layer">
      <div className="layer">{block()}</div>
    </div>
  );
}

export default hot(App);
